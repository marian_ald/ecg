close all;
clear all;

% incarc semnalul
struct = load('101m');

% extrag vectorul de valori
pulse = struct.('val');

% dimensiune semnal
dimens = length(pulse);
t = linspace(0, 1, dimens);

% semnal original
plot(t, pulse);

% aplic un filtru trece-sus, peantru a elimina frecventele mici
h = fir1(1000, 1 / 1000 * 2, 'high');
y_filt = filter(h, 1, pulse);

% filtru trece sus cu fereastra butterworth
%{
fc = 5; % Cut off frequency
fs = 1000; % Sampling rate
[b,a] = butter(6,fc/(fs/2), 'high'); % Butterworth filter of order 6
y_filt = filter(b, a, pulse); % Will be the filtered signal
%}


y_filt = fourfilt(pulse, 0.5, 100, 0.1);  %cea mai buna
%y_filt = fourfilt(pulse, 0.5, 20, 0.9);

figure;
plot(y_filt);


num = 15;
win = ones(1, num)/num;
%y_filt = conv(win, y_filt);
y_filt = filter(win,1, y_filt);


figure;
plot(y_filt);
