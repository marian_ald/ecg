% incarc structura semnalului
struct = load('100m');

% extrag vectorul de valori
pulse = getfield(struct, 'val');

len = length(pulse);
t = linspace(0, 1, len);

% valorile min si max ale semnalului
max_value = max(pulse)
min_value = min(pulse)

% semnal original
plot(t, pulse);

% normalizare
% acum valoarea minima a semnalului va fi 0, iar maximul 1
out = (pulse - min_value) / (max_value - min_value);

% semnal normalizat
figure;
plot(t, out);

% test
max_value = max(out)
min_value = min(out)


